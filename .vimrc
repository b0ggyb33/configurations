set rtp+=~/.fzf

set tabstop=2
set shiftwidth=2
set expandtab
set number
set hlsearch
set autoread

" noh will clear highlighting temporarily
set wildmenu

" force make to use tabs instead of spaces
autocmd FileType make setlocal noexpandtab


if !has('nvim')
  set mouse=a
  " https://superuser.com/questions/549930/cant-resize-vim-splits-inside-tmux
  set ttymouse=xterm2 
endif

set spell spelllang=en_gb

let g:ale_fixers = {'python': ['autopep8']}
let g:ale_linters = {'rust': ['analyzer']}
let g:ale_fix_on_save = 1
let g:ale_completion_enabled = 1

let g:rustfmt_autosave = 1

" load plugins
execute pathogen#infect()
syntax on
filetype plugin indent on

:call pathogen#helptags()

" enable lightline
set laststatus=2
" disable showing mode now lightline does it
set noshowmode
let g:lightline = {
  \ 'colorscheme': 'gruvbox',
  \ }

" Shortcut to toggle tagbar
nnoremap <silent> <Space>T :TagbarToggle<CR>

nmap <Space>F :Files<CR>
nmap <Space>f :GFiles<CR>
nmap <Space>b :Buffers<CR>
nmap <Space>t :Tags<CR>
nmap <Space>l :Lines<CR>
nmap <Space>L :BLines<CR>
nmap <Space>u :UndotreeToggle<CR>

" map <ctrl>-h,l to :sidewaysleft and right
nnoremap <c-h> :SidewaysLeft<cr>
nnoremap <c-l> :SidewaysRight<cr>

set background=dark
colorscheme gruvbox

let @n = '6GxGo'

" ctrl-v is an escape to allow you to write macros with special characters
let @x = 'iThis is the first line escaped with ctrl-v escoThis line has a continuationon a new line using ctrl-v-<CR>'
" @@ lets you redo the macro again
" ctrl-r twice lets you paste the register literally


"add gdb support using `Termdebug`
packadd termdebug
"for pico use:
"let g:termdebugger="gdb-multiarch"

set undofile
