FROM ubuntu:focal

RUN apt-get update
RUN apt-get upgrade -yq
RUN apt-get install ansible git make -yq
COPY * ./
RUN ansible-playbook playbook.yml
CMD /usr/bin/zsh
